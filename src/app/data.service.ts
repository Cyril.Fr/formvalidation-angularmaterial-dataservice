import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  dataForm: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() { }

  setData(data) {
    if (data) {
      console.log(data);
      this.dataForm.next(data);
    }
  }
}
