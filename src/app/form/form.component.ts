import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { DataService } from '../data.service';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public genders: string[] = ['Mr', 'Mme'];
  public checked = false;
  public registrationForm: FormGroup;
  public matcher = new MyErrorStateMatcher();
  public showCheckbox = false;
  public cityValue: string;

  public testValue = {
    firstName: 'yolo', lastName: 'toto', birthDate: new Date(),
    gender: 'Mr', password: 'totototo', zipCode: '69000', email: 'a@a.com', city: 'Lyon'};

  constructor(private formBuilder: FormBuilder, private dataService: DataService) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      zipCode: ['', [Validators.required, Validators.minLength(5), Validators.pattern('^[0-9]*$')]],
      email: ['', [Validators.required, Validators.email]],
      city: ['', [Validators.required]],
    });

    this.onChangeCityValue();
  }

  onSubmit() {
    console.log(this.registrationForm.value);
    this.dataService.setData(this.registrationForm.value);
    this.registrationForm.reset();
  }

  onChangeCityValue() {
    this.registrationForm.get('city').valueChanges.subscribe(
      val => {
        console.log(val.toLowerCase()),
        this.cityValue = val.toLowerCase(); }
    );
    console.log('city value : ', this.cityValue);

    /* if (this.cityValue === 'paris') {
      this.showCheckbox = true;
    } else {
      this.showCheckbox = false;
    } */

    this.cityValue === 'paris' ?
    this.showCheckbox = true :
    this.showCheckbox = false;
  }

  updatedForm() {
    this.registrationForm.setValue(this.testValue);
  }

}
