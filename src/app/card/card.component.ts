import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  formData: any;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.dataForm.subscribe(
      res => {
        console.log('formData card : ', res);
        this.formData = res; },
      error => { console.log(error); }
    );
  }

}
